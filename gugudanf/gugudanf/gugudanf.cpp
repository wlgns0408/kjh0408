﻿#include <stdio.h>

void main()
{
	int row, col;

	FILE *fp = NULL;

	fp = fopen("text.txt", "w");

	for (row = 1; row < 10; row++)
	{
		fprintf(fp, "%d단\n", row);

		for (col = 1; col < 10; col++)
		{
			fprintf(fp, "%d x %d = %d\t", row, col, row*col);
		}

		fprintf(fp, "\n\n");
	}

	fclose(fp);
}
