﻿#include <cstring>
#include <iostream>
#include "_car.h"


using namespace std;


int main(void)
{
	_car car;
	car.InitMember("car", 100);
	car.Accel();
	car.Accel();
	car.Accel();
	car.ShowCarState();
	car.Break();
	car.ShowCarState();
	car.Break();
	car.Break();
	car.ShowCarState();

}

