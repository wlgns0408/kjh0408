﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// set = 외부로부터 값을 받아오는(쓰기)역할을 함
// get = 받아온 값을 넘겨주는(읽기) 역할을 함

namespace C_Shop_Nine
{
    class User
    {
        private int user_Hp;
        private int user_Attack;
        public int Hp
        {
            get { return user_Hp; }
            set
            {
                user_Hp = value;
                Console.WriteLine("유저의 체력 : {0}", user_Hp);
            }
        }
        public int Attack
        {
            get { return user_Attack; }
            set
            {
                user_Attack = value;
                Console.WriteLine("유저의 공격력 : {0}", user_Attack);
            }
        }
    }
    class Monster
    {
        private int monster_hp;
        private int monster_Attack;

        public int HP
        {
            get { return monster_hp; }
            set
            {
                monster_hp = value;
                Console.WriteLine("몬스터의 체력 : {0}", monster_hp);
            }
        }
        public int Attack
        {
            get { return monster_Attack; }
            set
            {
                monster_Attack = value;
                Console.WriteLine("몬스터의 공격력 : {0}", monster_Attack);
            }
        }
        static void Main(string[] args)
        {
            User UU = new User();
            UU.Hp = 100;
            UU.Attack = 20;
            Monster MM = new Monster();
            MM.HP = 100;
            MM.Attack = 20;
            int B = 0;
            int A = 0;
            A = UU.Hp - MM.Attack;
            B = MM.HP - UU.Attack;
            Console.WriteLine("유저의 남은 피 : {0}", A);
            Console.WriteLine("몬스터의 남은 피 : {0}", B);

            Console.WriteLine(".");

            
        }
    }
}

/*
class NNNN
{
    private int num2;
    private int num;
    public int Num
    {
        get { return num; }
        set
        {
            num = value;
            Console.WriteLine("num의 값은 {0}", num);
        }
    }
    public int Num2
    {
        get { return num2; }
        set
        {
            num2 = value;
            Console.WriteLine("num2의 값은 {0}", num2);
        }
    }

    static void Main(string[] args)
    {
        NNNN n = new NNNN();
        n.Num = 100;
        n.Num2 = 101;

        int a = 0;
        a = n.num + n.num2;
        Console.WriteLine("두 값의 합은 : {0}", a);
   */

// -------------------------------------------------------------------
/*
class jihun
{
    private int num = 4;
    public int Num
    {
        get { return num; }
    }

    static void Main(string[] args)
    {
        jihun D = new jihun();
        Console.WriteLine(D.Num);
        Console.WriteLine(".");
    }
}
*/

