#include "FruitBuyer.h"

#include <iostream>

using namespace std;


FruitBuyer::FruitBuyer()
{
}

void FruitBuyer::Initmember(int money, int num)
{
	mymoney = money;
	numofApples = num;
}

void FruitBuyer::BuyApples(FruitSeller& seller, int money)
{
	numofApples += seller.Sellappels(money);
	mymoney -= money;
}

void FruitBuyer::ShowBuyer()
{
	cout << "구매자의 남은 잔액 : " << mymoney << endl;
	cout << "구매자의 구매한 사과  :" << numofApples << endl << endl;
}


FruitBuyer::~FruitBuyer()
{
}
