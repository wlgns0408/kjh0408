#include "FruitSeller.h"
#pragma once
class FruitBuyer
{
private:
	int mymoney;
	int numofApples;
public:
	void Initmember(int money, int num);
	void BuyApples(FruitSeller& seller, int money);
	void ShowBuyer();
	FruitBuyer();
	virtual ~FruitBuyer();
};

