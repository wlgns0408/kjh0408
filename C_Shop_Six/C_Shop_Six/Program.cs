﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Shop_Six
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] data = new int[2, 4];
            data[0, 0] = 1;
            data[0, 1] = 2;
            data[0, 2] = 3;
            data[0, 3] = 4;

            data[1, 0] = 5;
            data[1, 1] = 6;
            data[1, 2] = 7;
            data[1, 3] = 8;
            Console.WriteLine("{0} {1} {2} {3}", data[0, 0], data[0, 1], data[0,2], data[0,3]);
            Console.WriteLine("{0} {1} {2} {3}", data[1, 0], data[1, 1], data[1, 2], data[1, 3]);

            Console.WriteLine(".");
        }
    }
}
