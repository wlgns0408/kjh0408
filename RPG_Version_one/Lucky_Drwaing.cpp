﻿#include "Lucky_Drwaing.h"
#include <iostream>
#include <ctime>
#include <Windows.h>

using namespace std;

Lucky_Drawing::Lucky_Drawing()
{


}

void Lucky_Drawing::Initmember()
{
	numof_God = Lucky_CONST::God_num;
	numof_Wind = Lucky_CONST::Wind_num;
	numof_Fire = Lucky_CONST::Fire_num;
	numof_Water = Lucky_CONST::Water_num;
	numof_Up_Wind = Lucky_CONST::Up_Wind_num;
	numof_Up_Fire = Lucky_CONST::Up_Fire_num;
	numof_Up_Water = Lucky_CONST::Up_Water_num;
	numof_Broken = Lucky_CONST::Broken_num;
	numof_Fruit = Lucky_CONST::Fruit_num;
	numof_Leaf = Lucky_CONST::Leaf_num;

	Attack_Broken = Lucky_CONST::Broken_Attack;
	Attack_Fruit = Lucky_CONST::Fruit_Attack;
	Attack_Leaf = Lucky_CONST::Leaf_Attack;
	Attack_Wind = Lucky_CONST::Wind_Attack;
	Attack_Fire = Lucky_CONST::Fire_Attack;
	Attack_Up_Wind = Lucky_CONST::Up_Wind_Attack;
	Attack_Up_Fire = Lucky_CONST::Up_Fire_Attack;
	Attack_Up_Water = Lucky_CONST::Up_Water_Attack;
	Attack_God = Lucky_CONST::God_Attack;




}


void Lucky_Drawing::Lucky_Draw()
{
	int num = 0;

	Lucky_Drawing Lucky;

	while (1)
	{
		if (num == 3)
		{
			cout << "메뉴로 나갔습니다" << endl << endl;
			break;
		}
		srand((unsigned int)time(0));
		int random_number[25] = { 7, 7, 7,7, 8, 8, 8, 8, 9, 9, 9, 9, 1, 1, 2, 2, 3, 3, 4, 5, 6, 10 };
		int seed = rand();
		int rand = random_number[seed % 25];

		cin >> num;

		switch (num)
		{
		case 1:
			cout << "아이템 뽑는 중";
			Sleep(800);
			cout << ".";
			Sleep(800);
			cout << ".";
			Sleep(800);
			cout << ".";
			if (rand == 1)
			{
				cout << "++바람의 검을 뽑았습니다(레어)++" << endl;
				numof_Wind += 1;
				break;
			}
			else if (rand == 2)
			{
				cout << "++불의 검을 뽑았습니다(레어)++" << endl;
				numof_Fire += 1;
				break;
			}
			else if (rand == 3)
			{
				cout << "++물의 검을 뽑았습니다(레어)++" << endl;
				numof_Water += 1;
				break;
			}
			else if (rand == 4)
			{
				cout << "@@상향된 바람의 검을 뽑았습니다(영웅)@@" << endl;
				numof_Up_Wind += 1;
				break;
			}
			else if (rand == 5)
			{
				cout << "@@상향된 불의 검을 뽑았습니다(영웅)@@" << endl;
				numof_Up_Fire += 1;
				break;
			}
			else if (rand == 6)
			{
				cout << "@@상향된 물의 검을 뽑았습니다(영웅)@@" << endl;
				numof_Up_Water += 1;
				break;

			}
			else if (rand == 7)
			{
				cout << "--부서진 검을 뽑았습니다(일반)--" << endl;
				numof_Broken += 1;
				break;
			}
			else if (rand == 8)
			{
				cout << "--과도를 뽑았습니다(일반)--" << endl;
				numof_Fruit += 1;
				break;
			}
			else if (rand == 9)
			{
				cout << "--나뭇잎 검을 뽑았습니다(일반)--" << endl;
				numof_Leaf += 1;
				break;
			}
			else if (rand == 10)
			{
				cout << "@!@!@신의 검을 뽑았습니다(신화)@!@!@" << endl;
				numof_God += 1;
				break;
			}
		case 2:
			Lucky_Drawing::My_Inventory();
			break;
		default:
			break;
		}

	}
	
}

void Lucky_Drawing::My_Inventory()
{
	cout << "1.바람검 개수 : " << numof_Wind << endl;
	cout << "2.불의 검 개수 : " << numof_Fire << endl;
	cout << "3.물의 검 개수 : " << numof_Water << endl;
	cout << "4.상향된 바람검의 검 개수 : " << numof_Up_Wind << endl;
	cout << "5.상향된 불의 검 개수 : " << numof_Up_Fire << endl;
	cout << "6.상향된 물의 검 개수 : " << numof_Up_Water << endl;
	cout << "7.부서진 검의 개수 : " << numof_Broken << endl;
	cout << "8.과일 검의 개수 : " << numof_Fruit << endl;
	cout << "9.나뭇잎 검의 개수 : " << numof_Leaf << endl;
	cout << "0.신의 검 개수 : " << numof_God << endl;

}

/*void Fight_Monster::Select_Weapon()
{
	int num_ = 0;

	Lucky_Drawing Lucky;

	cout << "장착 하실 무기의 번호를 입력해주세요 : " << endl;
	cin >> num_;

	while (1)
	{
		if (num_ == 1)
		{
			if (Lucky.numof_Wind >= 1)
			{
				cout << "바람검을 장착합니다" << endl;
				user_Attack += Lucky_CONST::Wind_Attack;
				break;
			}
			else
			{
				cout << "바람검을 가지고 있지 않습니다" << endl;
			}
		}
		else if (num_ == 2)
		{
			if (Lucky.numof_Fire >= 1)
			{
				cout << "불의 검을 장착합니다" << endl;
				user_Attack += Lucky_CONST::Fire_Attack;
				break;
			}
			else 
				{
			}

		}
		else if (num_ == 3)
		{
			cout << "물의 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Water_Attack;
			break;
		}
		else if (num_ == 4)
		{
			cout << "향상된 바람의 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Up_Wind_Attack;
			break;

		}
		else if (num_ == 5)
		{
			cout << "향상된 불의 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Up_Fire_Attack;
			break;
		}
		else if (num_ == 6)
		{
			cout << "향상된 물의 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Up_Water_Attack;

			break;
		}
		else if (num_ == 7)
		{
			cout << "부서진 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Broken_Attack;
			break;
		}
		else if (num_ == 8)
		{
			cout << "과일 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Fruit_Attack;;
			break;
		}
		else if (num_ == 9)
		{
			cout << "나뭇잎 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::Leaf_Attack;

			break;
		}
		else if (num_ == 0)
		{
			cout << "신의 검을 장착합니다" << endl;
			user_Attack += Lucky_CONST::God_Attack;
			break;
		}
	}
}
*/



Lucky_Drawing::~Lucky_Drawing()
{

}

Fight_Monster::Fight_Monster()
{

}

void Fight_Monster::Initmember()
{
	Slime_Health = Lucky_CONST::Slime_HP;
	Demon_Health = Lucky_CONST::Demon_HP;
	Bone_Health = Lucky_CONST::Bone_HP;
	Mirror_Health = Lucky_CONST::Mirror_HP;
	Big_Slime_Health = Lucky_CONST::Big_Slime_HP;
	Big_Big_Slime_Health = Lucky_CONST::Big_Big_Slime_HP;
	User_Health = Lucky_CONST::User_HP;
	user_Attack = Lucky_CONST::User_Attack;
	Heal = Lucky_CONST::Heal_User;
	Attack_Slime = Lucky_CONST::ATTACK_Slime;
	Attack_Demon = Lucky_CONST::ATTACK_Demon;
	Attack_Bone = Lucky_CONST::ATTACK_Bone;
	Attack_Mirror = Lucky_CONST::ATTACK_Mirror;
	Attack_Big_slime = Lucky_CONST::ATTACK_Big_Slime;
	Attack_Big_Big_slime = Lucky_CONST::ATTACK_Big_Big_Slime;

	Skill_Attack_Slime = Lucky_CONST::SKILL_Slime;
	Skill_Attack_Demon = Lucky_CONST::SKILL_Demon;
	Skill_Attack_Bone = Lucky_CONST::SKILL_Bone;
	Skill_Attack_Mirror = Lucky_CONST::SKILL_Mirror;
	Skill_Attack_Big_Slime = Lucky_CONST::SKILL_Big_Slime;
	Skill_Attack_Big_Big_Slime = Lucky_CONST::SKILL_Big_Big_Slime;
}
void Fight_Monster::Slime_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[4] = { 1, 2, 3 ,4 };
	int seed = rand();
	int rand = random_number[seed % 4];

	if (rand == 1)
	{
		cout << "슬라임이 공격 합니다!" << endl;
		User_Health -= Attack_Slime;
		cout << "당신의 남은 피" << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "슬라임이 방어 합니다" << endl;
		cout << "하지만 슬라임이라 방어가 안됩니다.." << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "슬라임이 회복합니다" << endl;
		Slime_Health += Heal;
		cout << "슬라임의 남은 체력 : " << Slime_Health << endl << endl;
	}
	else if (rand == 4)
	{
		cout << "슬라임의 스킬 공격!" << endl;
		cout << "슬라임 짓누르기!" << endl;
		User_Health -= Skill_Attack_Slime;
		cout << "당신의 남은 피 :" << User_Health << endl << endl;
	}
}

void Fight_Monster::Demon_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[4] = { 1, 2, 2, 3 };
	int seed = rand();
	int rand = random_number[seed % 3];

	if (rand == 1)
	{
		cout << "데몬이 공격합니다 " << endl;
		User_Health -= Attack_Demon;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "데몬의 스킬 공격!" << endl;
		cout << "지옥불 던지기!" << endl;
		User_Health -= Skill_Attack_Demon;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "데몬의 피가 회복됩니다" << endl;
		Demon_Health += Heal;
		cout << "데몬의 남은 피 : " << Demon_Health << endl << endl;
	}
}

void Fight_Monster::Bone_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[3] = { 1, 2, 3 };
	int seed = rand();
	int rand = random_number[seed % 3];

	if (rand == 1)
	{
		cout << "해골병사가 당신을 공격합니다" << endl;
		User_Health -= Attack_Bone;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "해골병사가 스킬을 사용합니다" << endl;
		cout << "뼈다귀 부메랑!" << endl;
		User_Health -= Skill_Attack_Bone;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "해골병사가 힐 합니다" << endl;
		Bone_Health += Heal;
		cout << "해골 병사의 남은 피 : " << Bone_Health << endl << endl;
	}
}

void Fight_Monster::Mirror_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[3] = { 1, 2, 3 };
	int seed = rand();
	int rand = random_number[seed % 3];

	if (rand == 1)
	{
		cout << "거울이 공격합니다" << endl;
		User_Health -= Attack_Mirror;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "매직 미러 펀치!!" << endl;
		User_Health -= Skill_Attack_Mirror;
		cout << "당신의 남은 피  :" << User_Health << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "거울을 고칩니다" << endl;
		Mirror_Health += Heal;
		cout << "미러몬의 남은 피 : " << Mirror_Health << endl << endl;
	}

}

void Fight_Monster::Big_Slime_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[3] = { 1, 2, 3 };
	int seed = rand();
	int rand = random_number[seed % 3];


	if (rand == 1)
	{
		cout << "빅 슬라임이 공격합니다" << endl;
		User_Health -= Attack_Big_slime;
		cout << "당신의 남은 피 :" << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "빅 슬라임 스킬 사용 포식!" << endl;
		User_Health -= Skill_Attack_Big_Slime;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "빅 슬라임이 회복 합니다" << endl;
		Slime_Health += Heal;
		cout << "빅 슬라임의 남은 피 : " << Big_Big_Slime_Health << endl << endl;
	}

}

void Fight_Monster::Big_Big_Slime_Random_Pattern()
{
	srand((unsigned int)time(0));
	int random_number[7] = { 1, 1, 1, 1, 1, 2, 3 };
	int seed = rand();
	int rand = random_number[seed % 3];

	if (rand == 1)
	{
		cout << "빅 브라더 슬라임이 공격합니다" << endl;
		User_Health -= Attack_Big_Big_slime;
		cout << "당신의 남은 피" << User_Health << endl << endl;
	}
	else if (rand == 2)
	{
		cout << "빅브라더 슬라임의 징징짜기!" << endl;
		User_Health -= Skill_Attack_Big_Big_Slime;
		cout << "당신의 남은 피 : " << User_Health << endl << endl;
	}
	else if (rand == 3)
	{
		cout << "빅브라더 슬라임이 체력을 회복합니다" << endl;
		Big_Big_Slime_Health += Heal;
		cout << "빅브라더 슬라임의 남은 피" << Big_Big_Slime_Health << endl << endl;
	}

}

void Fight_Monster::Hunt_Slime()
{
	int Hunt_num = 0;

	while (1)
	{
		if (Slime_Health <= 0)
		{
			cout << "슬라임을 처치 했습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}
		

		
		else if (Hunt_num == 4)
		{
			break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.슬라임 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;

		
		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "슬라임을 공격!" << endl;
			Slime_Health -= user_Attack;
			cout << "슬라임 남은 피 :" << Slime_Health << endl;
			Slime_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Slime;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Slime_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Slime_Random_Pattern();
			
			break;

		case 4:
			Slime_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}

void Fight_Monster::Hunt_Demon()
{
	int Hunt_num = 0;

	while (1)
	{
		if (Demon_Health <= 0)
		{
		cout << "데몬을 처치하였습니다" << endl;
		cout << "메뉴로 돌아갑니다" << endl;
		break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (Hunt_num == 4)
		{
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.데몬 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;


		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "데몬을 공격!" << endl;
			Demon_Health -= user_Attack;
			cout << "데몬 남은 피 :" << Demon_Health << endl;
			Demon_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Demon;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Demon_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Demon_Random_Pattern();
			break;

		case 4:
			Demon_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}
void Fight_Monster::Hunt_Bone()
{
	int Hunt_num = 0;

	while (1)
	{
		if (Bone_Health <= 0)
		{
			cout << "해골병사를 처치 했습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (Hunt_num == 4)
		{
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.해골 병사 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;


		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "해골 병사를 공격!" << endl;
			Bone_Health -= user_Attack;
			cout << "해골 병사 남은 피 :" << Bone_Health << endl;
			Bone_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Bone;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Bone_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Bone_Random_Pattern();
			break;

		case 4:
			Bone_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}

void Fight_Monster::Hunt_Mirror()
{
	int Hunt_num = 0;

	while (1)
	{

		if (Mirror_Health <= 0)
		{
			cout << "미러몬 처치 했습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}
		if (Hunt_num == 4)
		{
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.미러몬 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;


		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "미러몬을 공격!" << endl;
			Mirror_Health -= user_Attack;
			cout << " 미러몬 남은 피 :" << Mirror_Health << endl;
			Mirror_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Mirror;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Mirror_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Mirror_Random_Pattern();
			break;

		case 4:
			Mirror_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}
void Fight_Monster::Hunt_Big_Slime()
{
	int Hunt_num = 0;

	while (1)
	{
		if (Big_Slime_Health <= 0)
		{
			cout << "빅슬라임을 처치 했습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}
		if (Hunt_num == 4)
		{
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.빅 슬라임 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;


		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "슬라임을 공격!" << endl;
			Big_Slime_Health -= user_Attack;
			cout << "빅 슬라임 남은 피 :" << Big_Slime_Health << endl;
			Big_Slime_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Big_slime;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Big_Slime_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Big_Slime_Random_Pattern();
			break;

		case 4:
			Big_Slime_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}
void Fight_Monster::Hunt_Big_Big_Slime()
{
	int Hunt_num = 0;

	while (1)
	{
		if (Big_Big_Slime_Health <= 0)
		{
			cout << "사실 저는...슬라임 맨!을 처치 했습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}

		if (User_Health <= 0)
		{
			cout << "체력이 없어 사망하였습니다" << endl;
			cout << "메뉴로 돌아갑니다" << endl;
			break;
		}
		if (Hunt_num == 4)
		{
			break;
		}
		cout << "무엇을 하시겠습니까?" << endl << endl;

		cout << "1.사실 저는...슬라임 맨! 공격" << endl;
		cout << "2.방어하기" << endl;
		cout << "3.휴식하기" << endl;
		cout << "4.도망치고, 메뉴로 돌아간다" << endl;

		cin >> Hunt_num;


		switch (Hunt_num)
		{
		case 1:
			cout << "공격을 선택하셧습니다" << endl;
			cout << "사실 저는...슬라임 맨!을 공격!" << endl;
			Big_Big_Slime_Health -= user_Attack;
			cout << "사실 저는...슬라임 맨! 남은 피 :" << Big_Big_Slime_Health << endl;
			Big_Big_Slime_Random_Pattern();
			break;

		case 2:
			cout << "방어를 선택하셧습니다" << endl;
			cout << "피해를 덜 받습니다" << endl;
			User_Health -= Attack_Big_Big_slime;
			cout << "당신의 남은 피  :" << User_Health << endl;
			Big_Big_Slime_Random_Pattern();
			break;

		case 3:
			cout << "휴식을 선택하셧습니다" << endl;
			cout << "휴식을 하여 체력이 약간 회복됩니다" << endl;
			User_Health += Heal;
			cout << "당신의 남은 피 : " << User_Health << endl;
			Big_Big_Slime_Random_Pattern();
			break;

		case 4:
			Big_Big_Slime_Random_Pattern();
			cout << "도망쳤습니다. 메뉴로 돌아갑니다" << endl;
			break;
		default:
			break;
		}
	}
}
void Fight_Monster::Fight_Random_Monster()
{
	while (1)
	{
		srand((unsigned int)time(0));
		int random_number1[21] = { 1, 1, 1, 1, 1, 1, 1,  2, 2, 2, 3, 3, 3, 3, 4, 4,4, 4, 5 ,5, 6 };
		int seed = rand();
		int rand = random_number1[seed % 21];

		if (rand == 1)
		{
			cout << "슬라임 출현!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Slime();
			break;
		}
		else if (rand == 2)
		{
			cout << "데몬 출현!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Demon();
			break;
		}
		else if (rand == 3)
		{
			cout << "해골 병사 출현!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Bone();
			break;
		}
		else if (rand == 4)
		{
			cout << "미러몬 출현!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Mirror();
			break;
		}
		else if (rand == 5)
		{
			cout << "빅 슬라임 출현!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Big_Slime();
			break;
		}
		else if (rand == 6)
		{
			cout << "사실 저는...슬라임 맨!" << endl;
			Fight_Monster Fight;
			Fight.Initmember();
			Fight.Hunt_Big_Big_Slime();
			break;
		}
	}
	
}



Fight_Monster::~Fight_Monster()
{

}