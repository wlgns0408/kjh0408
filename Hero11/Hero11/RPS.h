#pragma once
class RPSUser
{
private:
	int Rock;
	int Scissors;
	int Paper;

	int health;
	int Number_Of_Victories;
	int Numver_Of_Defeat;

public:
	void Initmemebr(int hp, int V_num, int D_num);

	void SHowList();

	//----------------------------------------------
	void If_U_Rock();
	int VICTORY(int V_num);
	int DEFEAT(int D_num, int hp);
	int Hurt_Monster(int hp);
	void ShowUserHP();
	//----------------------------------------------
	void If_U_Scissors();
	int _VICTORY(int V_num, int hp);
	int _DEFEAT(int D_num, int hp);
	int _Hurt_Monster(int hp);
	void _ShowUserHP();

	//----------------------------------------------
	void If_U_Paper();
	int __VICTORY(int V_num, int hp);
	int __DEFEAT(int D_num, int hp);
	int __Hurt_Monster(int hp);
	void __ShowUserHP();
	//----------------------------------------------


	RPSUser();
	virtual ~RPSUser();
};


//****************************************************************************************


class RPSMonster
{
private:
	int _Rock;
	int _Scissors;
	int _Paper;
	int _health;
public:
	void Initmember(int hp);

	//----------------------------------------------
	void If_M_Rock();
	int VICTORY(int V_num, int hp);
	int DEFEAT(int D_num, int hp);
	int Hurt_User(int hp);
	void ShowMonsterHP();
	//-----------------------------------------------
	void If_M_Scissors();
	int _VICTORY(int V_num, int hp);
	int _DEFEAT(int D_num, int hp);
	int _Hurt_User(int hp);
	void _ShowMonsterHP();
	//-----------------------------------------------
	void If_M_Paper();
	int __VICTORY(int V_num, int hp);
	int __DEFEAT(int D_num, int hp);
	int __Hurt_User(int hp);
	void __ShowMonsterHP();
	//----------------------------------------------

	void SHowMonsList();


	RPSMonster();
	virtual ~RPSMonster();
};

