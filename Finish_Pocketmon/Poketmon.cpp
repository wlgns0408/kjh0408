﻿#include "Poketmon.h"
#include <iostream>

using namespace std;


Poketmon::Poketmon()
{

}

void Poketmon::InitMember()
{
	Attack_water_poketmon = Poketmon_CONST::water_poketmon_ATTACK;
	Attack_fire_poketmon = Poketmon_CONST::fire_poketmon_ATTACK;
	Attack_pool_poketmon = Poketmon_CONST::pool_poketmon_ATTACK;

	Special_Attack_Water = Poketmon_CONST::Add_water_poketmon_ATTACK;
	Special_Attack_Fire = Poketmon_CONST::Add_fire_poketmon_ATTACK;
	Special_Attack_Pool = Poketmon_CONST::Add_pool_poketmon_ATTACK;

	Minimal_Effect_Water = Poketmon_CONST::Minus_water_poketmon_ATTACK;
	Minimal_Effect_FIre = Poketmon_CONST::Minus_fire_poketmon_ATTACK;
	Minimal_Effect_Pool = Poketmon_CONST::Minus_pool_poketmon_ATTACK;

	Water_health = Poketmon_CONST::WATER_Hp;
	Fire_health = Poketmon_CONST::FIRE_HP;
	Pool_health = Poketmon_CONST::POOL_HP;
}
void Poketmon::Water_Attack_Fire()
{
	cout << "물 타입의 공격!" << endl;
	Fire_health -= Poketmon_CONST::Add_water_poketmon_ATTACK;
	cout << "불 타입의 남은 피 : " << Fire_health;
	cout << "효과는 굉장했다!" << endl;
}

void Poketmon::Fire_Attack_Water()
{
	cout << "불 타입의 공격!" << endl;
	Water_health -= Poketmon_CONST::Minus_fire_poketmon_ATTACK;
	cout << "물 타입의 남은 피 : " << Water_health;
	cout << "효과는 미미했다..." << endl;
}

void Poketmon::Water_VS_Fire()
{
	cout << "물 타입과 불 타입이 싸웁니다" << endl;
	int num1;
	while (1)
	{
		if (Water_health < 0 || Fire_health < 0)
		{
			break;
		}
		cin >> num1;

		switch (num1)
		{
		case 1:
			Water_Attack_Fire();
			break;
		case 2:
			Fire_Attack_Water();
			break;
		default:
			break;
		}
	}
}

//-------------------------------------------------------------------------------------------------------------------


void Poketmon::Fire_Attack_Pool()
{
	cout << "불 타입의 공격!" << endl;
	Pool_health -= Poketmon_CONST::Add_fire_poketmon_ATTACK;
	cout << "풀 타입의 남은 피 : " << Pool_health;
	cout << "효과는 굉장했다!" << endl;
}

void Poketmon::Pool_Attack_Fire()
{
	cout << "풀 타입의 공격!" << endl;
	Fire_health -= Poketmon_CONST::Minus_pool_poketmon_ATTACK;
	cout << "불 타입의 남은 피 : " << Fire_health;
	cout << "효과는 미미했다..." << endl;
}

void Poketmon::FIre_VS_Pool()
{
	cout << "불 타입과 풀 타입이 싸웁니다" << endl;
	int num2;
	while (1)
	{
		if (Pool_health < 0 || Fire_health < 0)
		{
			break;
		}
		cin >> num2;

		switch (num2)
		{
		case 1:
			Fire_Attack_Pool();
			break;
		case 2:
			Pool_Attack_Fire();
			break;
		default:
			break;
		}
	}
}

// -----------------------------------------------------------------------------------------------------------------------

void Poketmon::Pool_Attack_Water()
{
	cout << "풀 타입의 공격!" << endl;
	Water_health -= Poketmon_CONST::Add_pool_poketmon_ATTACK;;
	cout << "물 타입의 남은 피 : " << Water_health;
	cout << "효과는 굉장했다!" << endl;
}

void Poketmon::Water_Attack_Pool()
{
	cout << "물 타입의 공격!" << endl;
	Pool_health -= Poketmon_CONST::Minus_water_poketmon_ATTACK;
	cout << "풀 타입의 남은 피 : " << Pool_health;
	cout << "효과는 미미했다..." << endl;
}

void Poketmon::Pool_VS_Water()
{
	cout << "풀 타입과 물 타입이 싸웁니다" << endl;
	int num3;
	while (1)
	{
		if (Pool_health < 0 || Water_health < 0)
		{
			break;
		}

		cin >> num3;

		switch (num3)
		{
		case 1:
			Pool_Attack_Water();
			break;
		case 2:
			Water_Attack_Pool();
			break;
		default:
			break;
		}
	}
}
Poketmon::~Poketmon()
{

}