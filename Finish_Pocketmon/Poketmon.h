﻿#pragma once
namespace Poketmon_CONST
{
	enum
	{
		water_poketmon_ATTACK = 20, fire_poketmon_ATTACK = 20, pool_poketmon_ATTACK = 20, Add_water_poketmon_ATTACK = 25, Add_fire_poketmon_ATTACK = 25,
		Add_pool_poketmon_ATTACK = 25, Minus_water_poketmon_ATTACK = 15, Minus_fire_poketmon_ATTACK = 15, Minus_pool_poketmon_ATTACK = 15,
		WATER_Hp = 100, FIRE_HP = 100, POOL_HP = 100, Water = 1, FIre = 2, Pool = 3
	};
}

// 물 > 불, 불 > 풀, 풀 > 물

class Poketmon
{
public:
	int Water_health;
	int Fire_health;
	int Pool_health;

	int Attack_water_poketmon;
	int Attack_fire_poketmon;
	int Attack_pool_poketmon;

	int Special_Attack_Water;
	int Special_Attack_Fire;
	int Special_Attack_Pool;

	int Minimal_Effect_Water;
	int Minimal_Effect_FIre;
	int Minimal_Effect_Pool;


public:
	void InitMember();

	void Water_VS_Fire();
	void FIre_VS_Pool();
	void Pool_VS_Water();

	void Water_Attack_Fire();
	void Fire_Attack_Water();

	void Fire_Attack_Pool();
	void Pool_Attack_Fire();

	void Pool_Attack_Water();
	void Water_Attack_Pool();


	Poketmon();
	virtual~Poketmon();


};








