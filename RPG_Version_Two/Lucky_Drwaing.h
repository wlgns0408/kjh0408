﻿#pragma once

namespace Lucky_CONST
{
	enum
	{
		God_num = 0, Wind_num = 0, Fire_num = 0, Water_num = 0, Up_Wind_num = 0, Up_Fire_num = 0, Up_Water_num = 0,
		Broken_num = 0, Fruit_num = 0, Leaf_num = 0, Broken_Attack = 10, Fruit_Attack = 20, Leaf_Attack = 30, Wind_Attack = 50, Fire_Attack = 60,
		Water_Attack = 60, Up_Water_Attack = 100, Up_Fire_Attack = 120, Up_Wind_Attack = 120, God_Attack = 300,
		
		SELECT_ = 0,

		Slime_Moster = 1, Demon_Monster = 2, Bone_Monster = 3, Mirror_Monster = 4, Big_Slime = 5, Big_Big_Slime = 6,
		Slime_HP = 100, Demon_HP = 200, Bone_HP = 150, Mirror_HP = 200, Big_Slime_HP = 500, Big_Big_Slime_HP = 1000, User_HP = 300,

		User_Attack = 50, Heal_User = 30, ATTACK_Slime = 10, ATTACK_Demon = 20, ATTACK_Bone = 30, ATTACK_Mirror = 50, 
		ATTACK_Big_Slime = 70, ATTACK_Big_Big_Slime = 100, 
		
		SKILL_Slime = 15, SKILL_Demon = 30, SKILL_Bone = 45, SKILL_Mirror = 75, 
		SKILL_Big_Slime = 105, SKILL_Big_Big_Slime = 1500
	};

}

class Lucky_Drawing 
{
public:
	int numof_God;
	int numof_Wind;
	int numof_Fire;
	int numof_Water;
	int numof_Up_Wind;
	int numof_Up_Fire;
	int numof_Up_Water;
	int numof_Broken;
	int numof_Fruit;
	int numof_Leaf;

	int select_wind_num;
	int select_fire_num;
	int select_water_num;
	int select_up_wind_num;
	int select_up_fire_num;
	int select_up_water_num;
	int select_Broken_num;
	int select_Fruit_num;
	int select_Leaf_num;
	int select_God_num;

public:
	int Attack_God;
	int Attack_Wind;
	int Attack_Fire;
	int Attack_Water;
	int Attack_Up_Wind;
	int Attack_Up_Fire;
	int Attack_Up_Water;
	int Attack_Broken;
	int Attack_Fruit;
	int Attack_Leaf;
public:
	void Initmember();
	void Lucky_Draw();
	void My_Inventory();
	void Select_Weapon();
	
	Lucky_Drawing();
	virtual ~Lucky_Drawing();


};

class Fight_Monster : public Lucky_Drawing
{
public:
	int user_Attack;

	Lucky_Drawing* Select = 0;

	int User_Health;
	int Slime_Health;
	int Demon_Health;
	int Bone_Health;
	int Mirror_Health;
	int Big_Slime_Health;
	int Big_Big_Slime_Health;

	int Attack_Slime;
	int Attack_Demon;
	int Attack_Bone;
	int Attack_Mirror;
	int Attack_Big_slime;
	int Attack_Big_Big_slime;

	int Skill_Attack_Slime;
	int Skill_Attack_Demon;
	int Skill_Attack_Bone;
	int Skill_Attack_Mirror;
	int Skill_Attack_Big_Slime;
	int Skill_Attack_Big_Big_Slime;
	int Heal;

	
public:
	void Initmember(Lucky_Drawing* p_Lucky);
	void Fight_Random_Monster(Lucky_Drawing* p_Lucky);
	void Hunt_Slime(Lucky_Drawing* p_Lucky);
	void Hunt_Demon(Lucky_Drawing* p_Lucky);
	void Hunt_Bone(Lucky_Drawing* p_Lucky);
	void Hunt_Mirror(Lucky_Drawing* p_Lucky);
	void Hunt_Big_Slime(Lucky_Drawing* p_Lucky);
	void Hunt_Big_Big_Slime(Lucky_Drawing* p_Lucky);
	void Slime_Random_Pattern();
	void Demon_Random_Pattern();
	void Mirror_Random_Pattern();
	void Bone_Random_Pattern();
	void Big_Slime_Random_Pattern();
	void Big_Big_Slime_Random_Pattern();



	Fight_Monster();
	virtual~Fight_Monster();

};

