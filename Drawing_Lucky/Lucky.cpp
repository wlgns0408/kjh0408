﻿#include "Lucky.h"
#include <iostream>
#include <ctime>

using namespace std;

Lucky::Lucky()
{

}

void Lucky::Initmember()
{
	num_of_Normal = Lucky_CONST::Normal_num;
	num_of_Rare = Lucky_CONST::Rare_num;
	num_of_Express = Lucky_CONST::Express_num;
	num_of_Legend = Lucky_CONST::Legend_num;
}

void Lucky::Rule()
{
	cout << "일반 등급이 10개거나" << endl << endl << "희귀 등급 7개거나" << endl << endl << "영웅등급 5개거나" << endl << endl <<
		"전설등급 3개일시" << endl << endl;
	cout << "프로그램이 종료 됩니다" << endl;
}


void Lucky::Show()
{
	cout << "당신의 일반등급 개수 : " << num_of_Normal << endl;
	cout << "당신의 희귀등급 개수 : " << num_of_Rare << endl;
	cout << "당신의 영웅등급 개수 : " << num_of_Express << endl;
	cout << "당신의 전설등급 개수 : " << num_of_Legend << endl;
}

void Lucky::Drawing()
{
	int num = 0;



	while (1)
	{
		if (num_of_Normal == 10 || num_of_Rare == 7)
		{
		
			break;
		}
		else if (num_of_Express == 5 || num_of_Legend == 3)
		{
			
			break;
		}

		srand((unsigned int)time(0));
		int random_number[10] = { 1, 1, 1, 1, 6, 3, 3, 3, 9, 6 }; // 일반등급 40%, 희귀등급 30%, 영웅등급 20%, 전설등급 10%
		int seed = rand();
		int rand = random_number[seed % 10];

		cin >> num;

		switch (num)
		{
		case 1:
			if (rand == 1)
			{
				cout << "일반 등급을 뽑았습니다!" << endl;
				num_of_Normal += 1;
				cout << "당신의 일반등급 개수 : " << num_of_Normal << endl << endl;
			
				break;
			}
			else if (rand == 3)
			{
				cout << "희귀 등급을 뽑았습니다!" << endl;
				num_of_Rare += 1;
				cout << "당신의 희귀등급 개수 : " << num_of_Rare << endl << endl;
			
				break;
			}
			else if (rand == 6)
			{
				cout << "영웅 등급을 뽑았습니다!" << endl;
				num_of_Express += 1;
				cout << "당신의 영웅등급 개수 : " << num_of_Express << endl << endl;
		
				break;
			}
			else
			{
				cout << "전설 등급을 뽑았습니다!" << endl;
				num_of_Legend += 1;
				cout << "당신의 전설등급 개수 : " << num_of_Legend << endl << endl;
				break;
			}
		default:
			break;
		}



	}
}



Lucky::~Lucky()
{

}
