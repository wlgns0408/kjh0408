﻿#include "Hit_Ball_Player.h"
#include <ctime>
#include <iostream>

using namespace std;

int main()
{
	int num = 0;

	Hit_Ball_Player Hit;
	Throw_Ball_Player Throw;

	srand((unsigned int)time(0));
	int random = rand() % 3 + 1;

	Hit.InitMember();
	Throw.Member();
	
	Hit.Hit_Ball_Player_List();
	while (1)
	{
		if (num == 4)
		{
			break;
		}

		cin >> num;

		switch (num)
		{
		case 1:
		{
			if (random == 1)
			{
				Hit.Safety_Score();
				Hit.Check_Safety_Score();
				Hit.Check_Score();
				break;
			}
			else
			{
				Throw.Strike_Score();
				Throw.Check_Strike_Score();
				Throw.Check_Out_Score();
				break;
			}
		}
		
		case 2:
		{
			if (random == 2)
			{
				Hit.Safety_Score();
				Hit.Check_Safety_Score();
				Hit.Check_Score();
				break;
			}
			else
			{
				Throw.Strike_Score();
				Throw.Check_Strike_Score();
				Throw.Check_Out_Score();
				break;
			}
		}
		
		case 3:
		{
			if (random == 3)
			{
				Hit.Safety_Score();
				Hit.Check_Safety_Score();
				Hit.Check_Score();
				break;
			}
			else
			{
				Throw.Strike_Score();
				Throw.Check_Strike_Score();
				Throw.Check_Out_Score();
				break;
			}
		}

		case 4:
			cout << "경기를 종료 합니다" << endl;
			break;
			break;
			
		default:
			break;
		}
	}
}