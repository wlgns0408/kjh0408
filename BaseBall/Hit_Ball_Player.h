﻿namespace BASEBALL_CONST
{
	enum
	{
		Fastball = 1, Curve = 2, Slide_Ball = 3, Out_ball_score = 0, Safe_Ball_score = 0, SCORE = 0, Strike_SCORE = 0
	};
}

class Hit_Ball_Player
{
public:
	int score;
	int safety_score;
public:
	void InitMember();
	void Hit_Ball_Player_List();
	void Safety_Score();
	void Check_Safety_Score();
	void Check_Score();
	Hit_Ball_Player();
	virtual ~Hit_Ball_Player();
};

class Throw_Ball_Player
{
public:
	int out_score;
	int strike_score;
public:
	void Member();
	void Strike_Score();
	void Check_Strike_Score();
	void Check_Out_Score();
	Throw_Ball_Player();
	virtual ~Throw_Ball_Player();

};
