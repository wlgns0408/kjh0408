﻿#include "Hit_Ball_Player.h"
#include <iostream>

using namespace std;


Hit_Ball_Player::Hit_Ball_Player()
{

}

void Hit_Ball_Player::InitMember()
{
	safety_score = BASEBALL_CONST::Safe_Ball_score;
	score = BASEBALL_CONST::SCORE;
}

void Hit_Ball_Player::Hit_Ball_Player_List()
{
	cout << "당신이 칠 수 있는 항목" << endl;
	cout << "1. 직구가 날라온다" << endl;
	cout << "2. 커브가 날라온다" << endl;
	cout << "3. 슬라이드볼이 날라온다" << endl;
}

void Hit_Ball_Player::Safety_Score()
{
	cout << "투수의 공을 읽고 안타를 쳤습니다!" << endl;
	cout << "안타 점수를 얻었습니다  ( + 1)" << endl;
	safety_score += 1;
}

void Hit_Ball_Player::Check_Safety_Score()
{
		if (safety_score == 4)
		{
			cout << "타자가 안타를 4번 쳤습니다!" << endl;
			cout << "득점 1점을 얻었습니다!" << endl;
			score += 1;
		}
		else if(safety_score == 8)
		{
			cout << "타자가 안타를 4번 쳤습니다!" << endl;
			cout << "득점 1점을 얻었습니다!" << endl;
			score += 1;
		}
	
}


void Hit_Ball_Player::Check_Score()
{
	if (score == 2)
	{
		cout << "2점을 득점 하였습니다!" << endl;
		cout << "타자가 우승하였습니다!" << endl;
	}
}
Hit_Ball_Player::~Hit_Ball_Player()
{

}






Throw_Ball_Player::Throw_Ball_Player()
{

}

void Throw_Ball_Player::Member()
{
	strike_score = BASEBALL_CONST::Strike_SCORE;
	out_score = BASEBALL_CONST::Out_ball_score;
}

void Throw_Ball_Player::Strike_Score()
{
	cout << "타자의 눈을 속여 스트라이크를 하였습니다!" << endl;
	cout << "스트라이크 점수 + 1" << endl;
	strike_score += 1;
}

void Throw_Ball_Player::Check_Strike_Score()
{
	if (strike_score == 3)
	{
		cout << "스트라이크 3번 달성!" << endl;
		cout << "아웃 점수 득점!" << endl;
		out_score += 1;
	}
	else if (strike_score == 6)
	{
		cout << "스트라이크 3번 달성!" << endl;
		cout << "아웃 점수 득점! " << endl;
		out_score += 1;
	}
	else if (strike_score == 9)
	{
		cout << "스트라이크 3번 달성!" << endl;
		cout << "아웃 점수 득점! " << endl;
		out_score += 1;
	}
	else if (strike_score == 12)
	{
		cout << "스트라이크 3번 달성!" << endl;
		cout << "아웃 점수 득점!" << endl;
		out_score += 1;
	}

}

void Throw_Ball_Player::Check_Out_Score()
{
	if (out_score == 4)
	{
		cout << "아웃을 4번 시켰습니다 ! " << endl;
		cout << "투수가 우승하였습니다!" << endl;
	}
}
Throw_Ball_Player::~Throw_Ball_Player()
{

}