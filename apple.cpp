﻿// 값을 입력받아서 사과를 예로한 사과장사꾼의 사과 개수, 돈, 사과 가격 그리고 사과 구매자의 현금. 사과개수를 표현하고
// 사과를 산 구매자의 현황과 사과를 판 판매자의 현황을 출력
#include <iostream>

using namespace std;

class FruitSeller
{
private:
	int mymoney;
	int numofapples;
	int apple_price;
public:

	void Initmenber(int money, int num, int price)
	{
		mymoney = money;
		numofapples = num;
		apple_price = price;
	}

	int SellApples(int money)
	{
		int num = money / apple_price;
		numofapples -= num;
		mymoney += money;
		return num;
	}

	void ShowSeller()
	{
		cout << "남은 사과의 개수 : " << numofapples << endl;
		cout << "벌은 수익 : " << mymoney << endl << endl;
	}
};

class FruitBuyer
{
private:
	int mymoney; 
	int numofapples;
public:

	void Initmember(int money)
	{
		mymoney = money;
		numofapples = 0;
	}

	void BuyApples(FruitSeller &seller, int money)
	{
		numofapples += seller.SellApples(money);
		mymoney -= money;
	}

	void ShowBuyer()
	{
		cout << "사과의 개수 :" << numofapples << endl;
		cout << "남은 돈 : " << mymoney << endl << endl;
	}
};

int main()
{
	FruitSeller seller;
	seller.Initmenber(0, 20, 1000);
	FruitBuyer buyer;
	buyer.Initmember(30000);
	buyer.BuyApples(seller, 5000);

	cout << "장사꾼의 현황" << endl;
	seller.ShowSeller();
	cout << "구매자의 현황" << endl;
	buyer.ShowBuyer();
}