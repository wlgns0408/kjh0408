﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace C_Shop_Seven
{
    class Program
    {
        class array
        {
            public int User_Hp;
            public int Monster_Hp;
            public int User_Attack;
            public int Monster_Attack;
            public string Monster_Hit = "퍽!";
            public string User_Hit = "슉!";
        }
        
        
        static void Main(string[] args)
        {
            int a;
            ArrayList al = new ArrayList();
            array ray = new array();

            al.Add(ray);
            ray.User_Hp = 100;
            ray.User_Attack = 15;
            ray.Monster_Attack = 15;
            ray.Monster_Hp = 100;
            
            Console.WriteLine("1.유저를 공격한다");
            Console.WriteLine("2.몬스터를 공격한다");
            while(true)
            {

                a = int.Parse(Console.ReadLine());


                
                
                switch (a)
                {
                    case 1:
                        Console.WriteLine("{0}", ray.Monster_Hit);
                        ray.User_Hp -= ray.Monster_Attack;
                        Console.WriteLine("유저를 공격합니다!");
                        Console.WriteLine("유저의 남은 피 : " + ((array)al[0]).User_Hp);
                        break;
            
                    case 2:
                        Console.WriteLine("{0}", ray.User_Hit);
                        ray.Monster_Hp -= ray.User_Attack;
                        Console.WriteLine("몬스터를 공격합니다!");
                        Console.WriteLine("몬스터의 남은 피 : " + ((array)al[0]).Monster_Hp);
                        break;
                }

                if (ray.User_Hp <= 0)
                {
                    Console.WriteLine("유저의 피가 0이하가 되어서 사망하였습니다");
                    break;
                }
                else if (ray.Monster_Hp <= 0)
                {
                    Console.WriteLine("몬스터의 피가 0이하가 되어서 처치하였습니다");
                    break;
                }

            }

            Console.WriteLine(".");

        }
    }
}
