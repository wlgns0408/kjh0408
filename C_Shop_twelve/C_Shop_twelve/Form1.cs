﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C_Shop_twelve
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(RedBox.Checked)
            {
                this.BackColor = Color.Red;
            }
            else
            {
                this.BackColor = DefaultBackColor;
            }
        }

        private void BlueBox_CheckedChanged(object sender, EventArgs e)
        {
            if(BlueBox.Checked)
            {
                this.BackColor = Color.Blue;
            }
            else
            {
                this.BackColor = DefaultBackColor;
            }
        }

        private void YellowBox_CheckedChanged(object sender, EventArgs e)
        {
            if (YellowBox.Checked)
            {

                this.BackColor = Color.Yellow;
            }
            else
            {
                this.BackColor = DefaultBackColor;
            }
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
            {
                 textBox1.BackColor = Color.Red;
            }
            else
            {
                textBox1.BackColor = DefaultBackColor;
            }
     
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton2.Checked)
            {
                textBox1.BackColor = Color.Blue;
            }
            else
            {
                textBox1.BackColor = DefaultBackColor;
            }
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton3.Checked)
            {
                textBox1.BackColor = Color.Yellow;
            }
            else
            {
                textBox1.BackColor = DefaultBackColor;
            }
        }

        private void CheckBox4_CheckedChanged_1(object sender, EventArgs e)
        {
            if(checkBox4.Checked)
            {
                this.BackColor = Color.Black;
            }
            else
            {
                this.BackColor = DefaultBackColor;
            }
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton4.Checked)
            {
                textBox1.BackColor = Color.Black;
            }
            else
            {
                textBox1.BackColor = DefaultBackColor;
            }
        }
    }
}
