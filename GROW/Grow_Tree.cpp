﻿#include "Grow_Tree.h"
#include <iostream>

using namespace std;

Grow_Tree::Grow_Tree()
{
}

void Grow_Tree::Initmember(int Exp)
{
	experience = Exp;
}

void Grow_Tree::Give_of_Tree()
{
	experience += GROW_CONST::Give_Water;
	cout << "나무에게 물을 주었습니다" << endl;
}

void Grow_Tree::Firtst_Grow()
{
	if (experience == GROW_CONST::First_Exp_Complete)
	{
		cout << "나무가 기본적인 형태를 잡게 되었습니다!" << endl;
	}
}

void Grow_Tree::Second_Grow()
{
	if (experience == GROW_CONST::Second_Exp_Complete)
	{
		cout << "나무에서 가지가 자라나게 되었습니다 !" << endl;
	}
}

void Grow_Tree::Third_Grow()
{
	if (experience == GROW_CONST::Third_Exp_Complete)
	{
		cout << "나뭇가지에서 잎이 자라나게 되었습니다 ! " << endl;
	}
}

void Grow_Tree::Finall_Grow()
{
	if (experience == GROW_CONST::Finall_Exp_Complete)
	{
		cout << "나무에서 열매가 생겼습니다 !" << endl;
		cout << "축하 합니다! 당신은 나무를 모두 키웠습니다!" << endl;
	}
}


Grow_Tree::~Grow_Tree()
{
}
