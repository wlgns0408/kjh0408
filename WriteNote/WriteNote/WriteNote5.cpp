// 나만의 5단계 연습해보기
// 살 수 있는 항목을 보여주고 번호 선택을 하면 그 물품을 구매후 내 잔액에서 빼고 내 물품의 개수에 더하기
//  또한 판매자와 구매자 모두 개수와 돈이 보이게 하기

#include <iostream>
using namespace std;

class ArticleSeller
{
private:
	int mymoney;

	int numofshirt;
	int numofpen;
	int numofapple;

	int shirt_price;
	int pen_price;
	int apple_price;

public:
	void Initmember(int money, int snum, int pnum, int anum, int sprice, int pprice, int aprice)
	{
		mymoney = money;

		numofshirt = snum;
		numofapple = anum;
		numofpen = pnum;

		shirt_price = sprice;
		pen_price = pprice;
		apple_price = aprice;
	}
	void Sellshirt(int money, int snum)
	{
		numofshirt -= snum;
		mymoney -= money - shirt_price;
	}
	void Sellpen(int money, int pnum)
	{
		numofpen -= pnum;
		mymoney -= money - pen_price;
	}
	void Sellapple(int money, int anum)
	{
		numofapple -= anum;
		mymoney -= money - apple_price;
	}

	void SellerNowResult()
	{
		cout << "-------------------------------------" << endl;
		cout << "판매자의 판매 이익 : " << mymoney << endl;
		cout << "판매자의 현재 옷 남은 개수 : " << numofshirt << endl;
		cout << "판매자의 현재 팬 남은 개수 : " << numofpen << endl;
		cout << "판매자의 현재 사과 남은 개수 : " << numofapple << endl << endl;
	}
	void ShowList()
	{
		cout << "당신이 살 수 있는 물품 목록입니다" << endl;
		cout << " 1. 옷 : " <<shirt_price<<"원"<< endl;
		cout << " 2. 팬 : " <<pen_price<<"원" <<endl;
		cout << " 3. 사과 : " << apple_price << "원" << endl << endl;
	}
};

class ArticleBuyer
{
private:
	int mymoney;
	int mynumof_shirt;
	int mynumof_pen;
	int mynumof_apple;
public:
	void Initmember(int money, int snum, int pnum, int anum)
	{
		mymoney = money;
		mynumof_shirt = snum;
		mynumof_pen = pnum;
		mynumof_apple = anum;
	}
	int BuyShirt(int money, int snum)
	{
		mynumof_shirt += snum;
		mymoney -= 8000;
		return snum;
	}
	int BuyPen(int money, int pnum)
	{
		mynumof_pen += pnum;
		mymoney -= 2000;
		return pnum;
	}
	int BuyApple(int money, int anum)
	{
		mynumof_apple += anum;
		mymoney -= 1000;
		return anum;
	}
	void ShowNowBuyer()
	{
		cout << "구매자의 현재 잔액 : " << mymoney << "원" << endl;
		cout << "구매자의 구매한 옷 개수 : " << mynumof_shirt << "개" << endl;
		cout << "구매자의 구매한 팬 개수 : " << mynumof_pen << "개" << endl;
		cout << "구매자의 구매한 사과 개수 : " << mynumof_apple << "개" << endl;
	}

};
// 구매자 클래스 아직 미완성 만들기

int main()
{
	int num = 0;

	ArticleSeller art;
	ArticleBuyer buy;
	art.Initmember(0, 20, 30, 25, 8000, 2000, 1000);
	buy.Initmember(100000, 0, 0, 0);
	art.ShowList();

	while (1)
	{
		cin >> num;

		switch (num)
		{
		case 1:
			art.Sellshirt(0, 1);
			buy.BuyShirt(0, 1);
			art.SellerNowResult();
			buy.ShowNowBuyer();
			break;

		case 2:
			art.Sellpen(0, 1);
			buy.BuyPen(0, 1);
			art.SellerNowResult();
			buy.ShowNowBuyer();
			break;

		case 3:
			art.Sellapple(0, 1);
			buy.BuyApple(0, 1);
			art.SellerNowResult();
			buy.ShowNowBuyer();
			break;

		default:
			cout << "구매를 종료" << endl;
			system("pause");
			break;
		}
	}
}