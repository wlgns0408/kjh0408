// 나만의 2단계 연습해보기
// 옷의 가격과 사이즈를 입력받아서 출력하기 ( 파리미터 x)

#include <iostream>
using namespace std;

class Shirt
{
private:
	int Shirt_price;
	int Shirt_size;
public:
	void Initmember();
};

void Shirt::Initmember()
{
	Shirt shr;
	cout << "옷의 가격은 얼마인가요 : ";
	cin >> shr.Shirt_price;
	cout << endl;
	cout << "옷의 사이즈는 몇인가요 :";
	cin >> shr.Shirt_size;

	cout << "옷의 가격은 " << shr.Shirt_price << "원 이고 옷의 사이즈는 " << shr.Shirt_size << "입니다" << endl;
}

int main()
{
	Shirt shr;
	shr.Initmember();
}