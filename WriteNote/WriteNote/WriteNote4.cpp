// 나만의 4단계 연습해보기
// 돈으로 티셔츠를 얼마나 살 수 있는지 확인하고 사고 내돈에서 감소하기
#include <iostream>
using namespace std;

class Shirt
{
private:
	int shirt_price;
	int numofshirt;
	int mymoney;
public:
	void Initmember(int price, int num, int money)
	{
		shirt_price = price;
		numofshirt = num;
		mymoney = money;
	}
	int Sellshirt(int money)
	{
		int num = mymoney / shirt_price;
		numofshirt += num;
		mymoney -= shirt_price * num;
		return num;
	}
	void ShowResult()
	{
		cout << "당신이 살 수 있는 옷의 개수 : " << numofshirt << endl;
		cout << "당신의 남은 잔액 : " << mymoney << endl;
	}
};
int main()
{
	Shirt sht;
	sht.Initmember(8000, 0, 50000);
	sht.Sellshirt(1000);
	sht.ShowResult();
}