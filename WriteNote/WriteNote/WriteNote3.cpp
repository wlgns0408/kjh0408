// 나만의 3단계 연습해보기
// 옷을 사고 난 후 나의 돈에서 옷의 가격만큼 빼기(옷의 가격은 아무거나로)

#include <iostream>
using namespace std;

class Shirt
{
private:
	int shirt_price;
	int mymoney;
public:
	void ShirtPrice(int price, int money);
};

void Shirt::ShirtPrice(int price, int money)
{
	Shirt shr;
	shr.shirt_price = price;
	shr.mymoney = money;
	cout << "티셔츠를 구매했습니다" << endl;
	cout << "당신의 남은 잔액은 : " << money - price << "입니다";

}

int main()
{
	Shirt shr;
	shr.ShirtPrice(8000, 50000);
}

