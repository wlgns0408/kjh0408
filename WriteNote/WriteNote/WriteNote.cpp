﻿// 나만의 1단계 연습해보기
// 옷 가격과 옷 사이즈를 파리미터로 입력받기

#include <iostream>
using namespace std;

class shirt
{
private:
	int shirt_price;
	int size;
public:
	void Initmember(int price, int money);
};

void shirt::Initmember(int price, int size)
{
	shirt_price = price;
	size = size;
	cout << "티셔츠 가격 : " << price << endl;
	cout << "티셔츠의 사이즈 : " << size;
}

int main()
{
	shirt shr;

	shr.Initmember(8000, 100);
}