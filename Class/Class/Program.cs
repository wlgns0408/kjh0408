﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Program
    {
       
        static void Plus()
        {
            int a, b;
            int c = 0;
            Console.WriteLine("더할 두개의 값을 입력해주세요 : ");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            c = a + b;
            Console.WriteLine("두 값의 합은 : {0}", c);
        }
        static void Minus()
        {
            int a, b;
            int c = 0;
            Console.WriteLine("뺄 두개의 값을 입력해주세요 : ");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            c = a - b;
            Console.WriteLine("두 값의 차는 : {0}", c);
        }
        static void Gop()
        {
            int a, b;
            int c = 0;
            Console.WriteLine("두 값을 곱할 두개의 수를 입력해주세요 : ");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            c = a * b;
            Console.WriteLine("두 값의 곱은 : {0}", c);
        }
        static void Nanugi()
        {
            int a, b;
            int c = 0;
            Console.WriteLine("두 값을 나눌 두개의 수를 입력해주세요 : ");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            c = a / b;
            Console.WriteLine("두 값을 나눈 몫은 : {0}", c);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("프로그램을 실행하시겠습니까?(Y/N)");
            string R = "Y";
            R = Console.ReadLine();

            while (R == "Y" || R == "y")
            {
                Console.WriteLine("입력할 기호를 선택해주세요");
                string C;
                C = Console.ReadLine();


                switch (C)
                {
                    case "+":
                        Plus();
                        break;

                    case "-":
                        Minus();
                        break;

                    case "*":
                        Gop();
                        break;  

                    case "/":
                        Nanugi();
                        break;

                    default:
                        Console.WriteLine("잘못된 기호를 입력하셨습니다");
                        break;
                }

                Console.WriteLine("계속 하시겠습니까?(Y/N)");
                R = Console.ReadLine();

                if(R == "Y" || R == "y")
                {
                    Console.WriteLine("계속 계산을 진행합니다");
                }
                else if(R == "N"|| R == "n")
                {
                    Console.WriteLine("계산을 멈추고 종료합니다");
                    break;
                }
                else
                {
                    Console.WriteLine("잘못된 기호를 입력하셨습니다");
                    Console.WriteLine("계속 하시겠습니까?(Y/N)");
                    R = Console.ReadLine();

                }

            }
          
            Console.WriteLine(".");
        }
    }
}
