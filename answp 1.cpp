﻿// 홀짝
//크기가 10인 배열 선언하고 10개정수를 입력
//홀수와 짝수구분 지어서 출력하는 프로그램 작성
#include "pch.h"
#include <iostream>

using namespace std;


int main()
{
	
	int arr[10];
	int i = 0;

	for (int i = 0; i < 10; i++)
	{
		cin >> arr[i];

		if (arr[i] % 2 == 1)
		{
			cout << "홀수" << endl;
		}
		else if (arr[i] % 2 == 0)
		{
			cout << "짝수" << endl;
		}
	}
}