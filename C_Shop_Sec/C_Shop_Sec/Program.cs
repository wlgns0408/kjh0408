﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Shop_Sec
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 10;
            int num2 = 20;

            Console.WriteLine(num + num2);
            Console.WriteLine(num - num2);
            Console.WriteLine(num * num2);
            Console.WriteLine(num2 / num);
            Console.WriteLine(num2 % num);

            Console.WriteLine(num2++);
            Console.WriteLine(--num2);
            Console.WriteLine(num2++);
            Console.WriteLine(num2++);
            Console.WriteLine(--num2);
            Console.WriteLine(--num2);

            int numtest = 1;
            numtest += num;
            Console.WriteLine(numtest);
            

            Console.WriteLine(".");
        }
    }
}
