﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Shop_eight
{
    class Program
    {
        class Jihun
        {
            public string name;
            
            public Jihun(string _name)  // 생성자
            {
                name = _name;

            }
            public void Call() // 종료자
            {
                Console.WriteLine(this.name);
            }

        }
        static void Main(string[] args)
        {
            Jihun C = new Jihun("지훈");
            Console.WriteLine(C.name);
            C.Call();

            Console.WriteLine(".");

        }
    }
}
