﻿#include "Item.h"
#include <iostream>

using namespace std;

int main()
{
	Item I;

	int num = 0;

	I.Initmember(1000);

	I.Item_LIST();

	while (1)
	{
		cin >> num;

		switch (num)
		{
		case 1:
			I.Buy_Sword();
			I.Item_LIST();
			break;
		case 2:
			I.Buy_Potion();
			I.Item_LIST();
			break;
		case 3:
			I.Buy_Armor();
			I.Item_LIST();
			break;
		case 4:
			I.Buy_High_Cloth();
			I.Item_LIST();
			break;
		case 5:
			I.Buy_Low_Cloth();
			I.Item_LIST();
			break;
		case 6:
			I.Buy_Shoes();
			I.Item_LIST();
			break;
		case 7:
			I.Buy_Hat();
			I.Item_LIST();
			break;
		case 8:
			I.Show_My_Inventory();
			break;

		default:
			break;
		}
	}
}