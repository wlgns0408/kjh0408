﻿#include "Item.h"
#include <iostream>

using namespace std;

Item::Item()
{

}

void Item::Initmember(int money)
{
	mymoney = money;
	my_num_of_Sword = Item_CONST::My_numof_Sword;
	my_num_of_Potion = Item_CONST::My_numof_Potion;
	my_num_of_Armor = Item_CONST::My_numof_Armor;
	my_num_of_High_Cloth = Item_CONST::MY_numof_High_Cloth;
	my_num_of_Low_Cloth = Item_CONST::My_numof_Low_cloth;
	my_num_of_Shoes = Item_CONST::My_numof_Shoes;
	my_num_of_Hat = Item_CONST::My_numof_Hat;
}

void Item::Item_LIST()
{
	cout << "1. 검 : " << Item_CONST::Sword_PRICE << endl;
	cout << "2. 포션 : " << Item_CONST::Potion_PRICE << endl;
	cout << "3. 방패 : " << Item_CONST::Armor_PRICE << endl;
	cout << "4. 상의 : " << Item_CONST::High_Cloth_PRICE << endl;
	cout << "5. 하의 : " << Item_CONST::Low_Cloth_PRICE << endl;
	cout << "6. 신발 : " << Item_CONST::Shoes_PRICE << endl;
	cout << "7. 모자 : " << Item_CONST::Hat_Price << endl;
}

void Item::Buy_Sword()
{
	cout << "검을 구매했습니다" << endl;
	my_num_of_Sword += 1;
	mymoney -= Item_CONST::Sword_PRICE;
}

void Item::Buy_Potion()
{
	cout << "포션을 구매했습니다" << endl;
	my_num_of_Potion += 1;
	mymoney -= Item_CONST::Potion_PRICE;
}

void Item::Buy_Armor()
{
	cout << "방패을 구매했습니다" << endl;
	my_num_of_Armor += 1;
	mymoney -= Item_CONST::Armor_PRICE;
}

void Item::Buy_High_Cloth()
{
	cout << "상의을 구매했습니다" << endl;
	my_num_of_High_Cloth += 1;
	mymoney -= Item_CONST::High_Cloth_PRICE;
}

void Item::Buy_Low_Cloth()
{
	cout << "하의을 구매했습니다" << endl;
	my_num_of_Low_Cloth += 1;
	mymoney -= Item_CONST::Low_Cloth_PRICE;
}

void Item::Buy_Shoes()
{
	cout << "신발을 구매했습니다" << endl;
	my_num_of_Shoes += 1;
	mymoney -= Item_CONST::Shoes_PRICE;
}

void Item::Buy_Hat()
{
	cout << "모자을 구매했습니다" << endl;
	my_num_of_Hat += 1;
	mymoney -= Item_CONST::Hat_Price;
}

void Item::Show_My_Inventory()
{
	cout << "당신이 가지고 있는 검의 개수 : " << my_num_of_Sword << endl;
	cout << "당신이 가지고 있는 포션의 개수 : " << my_num_of_Potion << endl;
	cout << "당신이 가지고 있는 방패의 개수 : " << my_num_of_Armor << endl;
	cout << "당신이 가지고 있는 상의의 개수 : " << my_num_of_High_Cloth << endl;
	cout << "당신이 가지고 있는 하의의 개수 : " << my_num_of_Low_Cloth << endl;
	cout << "당신이 가지고 있는 신발의 개수 : " << my_num_of_Shoes << endl;
	cout << "당신이 가지고 있는 모자의 개수 : " << my_num_of_Hat << endl;
}

Item::~Item()
{

}