﻿#pragma once

namespace Item_CONST
{
	enum
	{
		Sword_PRICE = 500, Potion_PRICE = 20, Armor_PRICE = 400, High_Cloth_PRICE = 200, Low_Cloth_PRICE = 150,
		Shoes_PRICE = 100, Hat_Price = 100, My_numof_Sword = 0, My_numof_Potion = 0, My_numof_Armor = 0,
		MY_numof_High_Cloth = 0, My_numof_Low_cloth = 0, My_numof_Shoes = 0, My_numof_Hat = 0
	};
}
class Item
{
private:
	int mymoney;
	int my_num_of_Sword;
	int my_num_of_Potion;
	int my_num_of_Armor;
	int my_num_of_High_Cloth;
	int my_num_of_Low_Cloth;
	int my_num_of_Shoes;
	int my_num_of_Hat;
public:
	void Initmember(int money);

	void Item_LIST();

	void Buy_Sword();
	void Buy_Potion();
	void Buy_Armor();
	void Buy_High_Cloth();
	void Buy_Low_Cloth();
	void Buy_Shoes();
	void Buy_Hat();

	void Show_My_Inventory();

	Item();
	virtual ~Item();

};

